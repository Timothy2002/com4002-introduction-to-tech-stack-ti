from flask import Flask, render_template, url_for

app = Flask(__name__)

@app.route('/')
def home(): # the home page of the website
    return render_template('home.html', page="Home")

@app.route('/electronics')
def electronics(): # the electronic page of the website
    return render_template('electronics.html', page="Electronic")

@app.route('/clothing')
def clothing(): # the clothing page of the website
    return render_template('clothing.html', page="Clothing")

@app.route('/about')
def about(): # the about page of the website
    return render_template('about.html', page="About")

app.run(host="0.0.0.0", debug=True)
