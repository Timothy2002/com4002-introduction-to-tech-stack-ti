DROP DATABASE bet8888;
CREATE DATABASE bet8888;
USE bet8888;
DROP TABLE IF EXISTS Category, Bet, Picture, Game, User;




CREATE TABLE User (
	id INT AUTO_INCREMENT,
	email VARCHAR(45) UNIQUE NOT NULL,
	username VARCHAR(15) UNIQUE NOT NULL,
    is_admin BOOLEAN DEFAULT 0,
	password VARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;

INSERT INTO User (email, username, password, is_admin) VALUES
  ("bob@bob.com", "scarface", "def123B-2", 0),
  ("goofy@hotmail.com", "goofy", "g00f8a11", 0),
  ("lala@tinkywinky.com", "lala", "ttsay88", 0),
  ("shushu@gmail.com", "shushu", "jiggy_66", 0),
  ("admin@ltucook.com", "admin", "pass123", 1);




CREATE TABLE Category (
	id INT AUTO_INCREMENT,
	name VARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;

INSERT INTO Category (name) VALUES
	("Virtual Sports"),
	("Racing"),
	("Jackpots"),
	("Poker"),
	("Casino"),
	("Slots");




CREATE TABLE Game (
	id INT AUTO_INCREMENT,
	user_id INT,
	cat_id INT,
	title VARCHAR(50) NOT NULL,
	number_game VARCHAR(500),
	number_game1 VARCHAR(500),
	when_posted TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    title_game VARCHAR(500),
	title_game1 VARCHAR(500),
	img_url VARCHAR(200),
	PRIMARY KEY (id),
    FOREIGN KEY (cat_id) REFERENCES Category (id),
    FOREIGN KEY (user_id) REFERENCES User (id)
) ENGINE=InnoDB;

INSERT INTO Game (title, title_game, title_game1, number_game, number_game1, cat_id, user_id, img_url) VALUES
("Football", "Liverpool vs Newcastle", "Chelsea vs Manchester United", 12, 22, 4, 1, "images/Football1.png"),
("American Football", "Philadelphia Eagles vs Tampa Bay Buccaneers", "New England Patriots vs Las Vegas Raiders", 24, 23, 2, 2, "images/Afootball.png"),
("Basketball", "New York Yankees vs Cleveland Indians", "Boston Red Sox vs Chicago Cubs", 8, 11, 2, 3, "images/Basketball.png"),
("Esports", "League of Legends", "Dota 2", 19, 28, 2, 2, "images/Esports.png"),
("Boxing", "Mike Tyson vs Muhammad Ali", "Floyd Mayweather vs Manny Pacquiao", 20, 29, 2, 1, "images/Boxing.png"),
("Ice Hockey", "Telford Tigers vs Calgary Flames", "Washington Capitals vs Toronto Maple Leafs", 22, 10, 3, 5, "images/Ice Hockey.png");
    



CREATE TABLE Bet (
	id INT AUTO_INCREMENT,
	bet_id INT NOT NULL,
	item_id INT NOT NULL,
	amount DECIMAL(4,2) UNSIGNED,
	PRIMARY KEY (id),
    FOREIGN KEY (bet_id) REFERENCES User (id),
    FOREIGN KEY (item_id) REFERENCES Item (id)
) ENGINE=InnoDB;

INSERT INTO Bet (bet_id, item_id, amount) VALUES
	(4, 1, 9.50),
    (4, 2, 21.00),
	(3, 2, 13.20),
	(3, 1, 16.00),
	(5, 2, 3.10);
