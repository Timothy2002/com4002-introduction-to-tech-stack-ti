from flask import Flask, url_for, render_template
import mysql.connector
  
config = {
  	'user': 'root',
  	'password': 'root',
  	'host': 'db',
  	'port': '3306',
  	'database': 'bet8888'
  }

app = Flask(__name__)
 
@app.route("/")
def home():
        #route for the home view
           return render_template('home.html', page_name="Home")

@app.route('/login')
def login():
    #route for the login view
           return render_template('login.html', page_name="Login")

@app.route('/registration')
def registration():
        #route for the registration view
           return render_template('registration.html', page_name="registration")

@app.route('/checkout')
def checkout():
        #route for the checkout view
           return render_template('checkout.html', page_name="checkout")

@app.route('/v_sports')
def v_sports():
    #route for the vertual sports view
  
    # connects to mysql database and fetch data for the bets
    db = mysql.connector.connect(**config)
    cursor = db.cursor()
    query = "SELECT Game, title_game, title_game1, number_game, number_game1, img_url FROM Game WHERE cat_id=2"
    cursor.execute(query)
    rows = cursor.fetchall()
    return render_template('v_sports.html', page_name="v_sports", data=rows)
    

app.run(host="0.0.0.0", debug=True)
