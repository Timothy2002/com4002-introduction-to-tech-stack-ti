DROP DATABASE unibay;
CREATE DATABASE unibay;
USE unibay;
DROP TABLE IF EXISTS Category, Bid, Picture, Item, User;

CREATE TABLE User (
	id INT AUTO_INCREMENT,
	email VARCHAR(45) UNIQUE NOT NULL,
	username VARCHAR(15) UNIQUE NOT NULL,
    is_admin BOOLEAN DEFAULT 0,
	password VARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE Category (
	id INT AUTO_INCREMENT,
	name VARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE Item (
	id INT AUTO_INCREMENT,
	user_id INT,
	cat_id INT,
	title VARCHAR(50) NOT NULL,
    asking_price DECIMAL(4,2) UNSIGNED,
	when_posted TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    description VARCHAR(500),
	img_url VARCHAR(200),
	PRIMARY KEY (id),
    FOREIGN KEY (cat_id) REFERENCES Category (id),
    FOREIGN KEY (user_id) REFERENCES User (id)
) ENGINE=InnoDB;

CREATE TABLE Bid (
	id INT AUTO_INCREMENT,
	bidder_id INT NOT NULL,
	item_id INT NOT NULL,
	amount DECIMAL(4,2) UNSIGNED,
	PRIMARY KEY (id),
    FOREIGN KEY (bidder_id) REFERENCES User (id),
    FOREIGN KEY (item_id) REFERENCES Item (id)
) ENGINE=InnoDB;

INSERT INTO User (email, username, password, is_admin) VALUES
  ("bob@bob.com", "scarface", "def123B-2", 0),
  ("goofy@hotmail.com", "goofy", "g00f8a11", 0),
  ("lala@tinkywinky.com", "lala", "ttsay88", 0),
  ("shushu@gmail.com", "shushu", "jiggy_66", 0),
  ("admin@ltucook.com", "admin", "pass123", 1);

INSERT INTO Category (name) VALUES
	("household"),
	("clothing"),
	("electrical"),
	("footwear"),
	("sports equipment");
    
INSERT INTO Item (title, description, asking_price, cat_id, user_id, img_url) VALUES
("Chelsea Boots", "Size 7 hardly worn.", 20.00, 4, 1, "images/chelsea-boot.jpg"),
("Yellow Skirt", "Size 10 lovely summery skirt.", 4.99, 2, 2, "images/skirt.jpg"),
("Grey Hoodie", "Size M. Unwanted gift. Organic cotton.", 15.00, 2, 3, "images/hoodie.jpg"),
("Red Cord Trousers", "Jazzy red cords. Size S.", 4.99, 2, 2, "images/red-cords.jpg"),
("Ear Muffs", "Keep your ears warm with these cosy ear muffs.", 1.99, 2, 1, "images/muffs.jpg"),
("HTC One Mini Unlocked", "Used phone in good used condition. Sim free. No memory included.", 99.99, 3, 5, "images/phone.jpg");
    
INSERT INTO Bid (bidder_id, item_id, amount) VALUES
	(4, 1, 9.50),
    (4, 2, 21.00),
	(3, 2, 13.20),
	(3, 1, 16.00),
	(5, 2, 3.10);
