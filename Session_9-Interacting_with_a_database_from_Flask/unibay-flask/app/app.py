from flask import Flask, url_for, render_template
import mysql.connector

config = {
  	'user': 'root',
  	'password': 'root',
  	'host': 'db',
  	'port': '3306',
  	'database': 'unibay'
  }


app = Flask(__name__)

@app.route('/')
def home():
    """ route for the home view
    """
    return render_template('home.html', page_name="Home")

@app.route('/clothes')
def clothes():
    """ route for the clothes view
    """
    # TODO: connect to mysql database and fetch clothing data
    db = mysql.connector.connect(**config)
    cursor = db.cursor()
    query = "SELECT title, description, img_url FROM Item WHERE cat_id=2"
    cursor.execute(query)
    rows = cursor.fetchall()
    return render_template('clothes.html', page_name="Clothes", data=rows)


app.run(host='0.0.0.0', debug=True)
