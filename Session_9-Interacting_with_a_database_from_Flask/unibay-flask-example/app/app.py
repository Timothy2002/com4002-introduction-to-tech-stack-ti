from flask import Flask, url_for, render_template
from db import db_connect, db_query

app = Flask(__name__)

@app.route('/')
def home():
    """ route for the home view
    """
    return render_template('home.html', page_name="Home")

@app.route('/clothes')
def clothes():
    """ route for the clothes view
    """
    # connect to database
    db = db_connect()
    if db:
        data = db_query(db, "SELECT * FROM Item WHERE cat_id = 2")
    else:
        data = [] 
    return render_template('clothes.html', page_name="Clothes", data=data)

app.run(host='0.0.0.0', debug=True)
