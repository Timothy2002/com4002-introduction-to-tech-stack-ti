import mysql.connector
from mysql.connector import errorcode

# store some configurables
# for connecting to database
config = {
    'user': 'root',
    'password': 'root',
    'host': 'db',
    'port': '3306',
    'database': 'unibay'
}

# https://dev.mysql.com/doc/connector-python/en/

def db_connect():
    """ function establishes connection with
        a mysql database
    """
    try:
        db = mysql.connector.connect(**config)
    except mysql.connector.Error as err:
        return False
    else:
        return db

def db_query(db, query):
    """ function tries to execute query on 
        database. returns results as list
        of python dictionaries.
    """
    try:
        cursor = db.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        field_names = [i[0] for i in cursor.description]
    except mysql.connector.Error as err:
        return err
    else:
        return rows_to_dicts(rows, field_names)

def db_close(db):
    db.close()
    return

def rows_to_dicts(rows, field_names):
    """ function turns tuples and field names
        into list of dictionaries
    """
    data = []
    for i, row in enumerate(rows):
        data.append({field:row[j] for j, field in enumerate(field_names) })
    return data
