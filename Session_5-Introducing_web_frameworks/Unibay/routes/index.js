var express = require('express');
var router = express.Router();

// import the fs package from node
var fs = require("fs");

// read in the json data from external file
var data = fs.readFileSync("./data/data.json");

// parse json data
var json = JSON.parse(data);


/* GET home page */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home' });
});

/* GET clothes page */
router.get('/clothes', function (req, res, next) {
    res.render('clothes', { title: 'Clothes', data: json });
});


module.exports = router;
