// fetchs the express package and create an instance of an express app
// Here the constant variable called app is an object and it has predefined properties and methods associated with it
const express = require('express')
  const app = express()

// creates a variable to store the port number on the server where this app will be served
  const port = 3000

// defines a URL end point for this application, which will be the application root. 
//The application will now handle GET requests which are made to this end point by responding with the text, 'Hello World!'
  app.get('/', (req, res) => {
    res.send('Hello World!')
  })

// tell the app to start a server and listen on port 3000 for connections
  app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })
