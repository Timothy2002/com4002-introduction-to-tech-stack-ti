//this loads in the data from the data javascript
var resultsContainer = document.getElementById("results");

var html = "";

for (var i = 0; i < Electronics.length; i++) {
	if (i % 3 == 0 || i == 0) {
	}
	html += "<div class='col-md-4'><div class='Products-for-sale'>";
	html += "<h4>" + Electronics[i].title + "</h4>";
	html += "<div class='Product-image'>";
	html += "<img src='Image/" + Electronics[i].imageUrls[0] + "'/>";
	html += "</div>";
	html += "<p>" + Electronics[i].description + "</p>";
	html += "<p>" + Electronics[i].cost + "</p>";
	html += "<br class='clearfix'/>";
	html += "</div></div>";
	if ((i + 1) % 3 == 0) {
		html += "</div>";
	}
}

resultsContainer.innerHTML = html;