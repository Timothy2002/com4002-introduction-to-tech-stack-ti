from flask import Flask, url_for, render_template, request
from db import get_items_by_cat, get_items

app = Flask(__name__)

@app.route('/')
def home():
    """ route for the home view
    """
    return render_template('home.html', page_name="Home")

@app.route('/items')
def items():
    """ generic route for items
    """
    category = request.args.get('cat')
    if category is not None:
        try:
            data = get_items_by_cat(category)
        except:
            data = []
    else:
        try:
            data = get_items()
        except:
            data = []
    return render_template('results.html', page_name=category, data=data)
app.run(host='0.0.0.0', debug=True)
