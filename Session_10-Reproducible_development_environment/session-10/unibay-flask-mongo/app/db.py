from pymongo import MongoClient

def db_connect():
    """ function establishes connection with
        a mongo database
    """
    try:
        # connect to instance of mongodb
        client = MongoClient('mongodb://db:27017')
        # select a database to use
        db = client.unibay
    except:
        return False
    else:
        return db

def get_items_by_cat(cat):
    """ function retrieves items
        from database by category.
    """
    db = db_connect()
    if db:
        items = db.items.find({"category":cat})
    else:
        items = []
    return items

def get_items():
    """ function retrieves all items
        from database.
    """
    db = db_connect()
    if db:
        items = db.items.find()
    else:
        items = []
    return items
