# Unibay App 
## Implemented with Flask and MySQL on Docker

This partial implementation of the Unibay app is built with the Flask microframework. The database backend is MySQL. It runs inside Docker.

+ All of the application code (the python) is in the `app/` directory
+ The SQL statements required to setup the database is in `db/setup.sql`

**This version of the application is an example solution to the Session 9 activities, but try the activities first before coming here!**

### Install

To build and run the application, make sure Docker is running first.

+ From Windows Powershell (in this location), run:

		docker-compose up --build
The above command will build the application according to the 'recipe' dictated in `docker-compose.yaml`.

+ Visit the app in the browser: [localhost:5000](http://localhost:5000)

To run the app in detached mode (i.e. in the background), do:

		docker-compose up --build -d

### Making changes to the application source code

You can edit the source code, but you will need to rebuild the application after editing. This is because the running version of the application is actually a copy of the source code which is running in the docker container (hence editing the files here won't have any effect.)

Simply repeat the installation step above each time you make changes to the source code.

### Making changes to the database

You can edit the SQL statements in `db/setup.sql` if you want to change the database schema or add different data.

However, you need to make sure the database is recreated for the changes to have any effect. 

The simplest way to do this is to destroy the container running the database, and rebuild the app.

		docker rm unibay-mysql
		docker-compose up --build

### Happy hacking!