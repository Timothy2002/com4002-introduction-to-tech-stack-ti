from flask import Flask, url_for, render_template
import mysql.connector

app = Flask(__name__)

config = {
    'user': 'root',
    'password': 'root',
    'host': 'db',
    'port': '3306',
    'database': 'unibay'
}


@app.route('/')
def home():
    return render_template('home.html', page_name="Home")

@app.route('/clothes')
def clothes():
    db = mysql.connector.connect(**config)
    cursor = db.cursor()
    query = "SELECT title, description, price, img_url FROM Item WHERE cat_id=2"
    cursor.execute(query)
    rows = cursor.fetchall()
    return render_template('clothes.html', page_name="Clothes", data=rows)

app.run(host='0.0.0.0', debug=True)
