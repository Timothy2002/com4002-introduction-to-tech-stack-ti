# practice queries

USE unibay;

SELECT title, description, asking_price FROM Item;
SELECT bidder_id, item_id, amount FROM Bid WHERE item_id = 2 ORDER BY bid_amount DESC;
SELECT title, description FROM Item WHERE cat_id = 2 AND asking_price < 10.00;
