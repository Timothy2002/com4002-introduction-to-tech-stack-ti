# Session 6: Database technologies

In this directory you'll find JSON and SQL resources accompanying the Session 6 activities detailed [here](https://gitlab.com/LTUcompsci/4002-intro-to-tech-stack/-/wiki_pages/session-6). Please note that the lab instructions assume you have docker installed on your system. You can either use your Virtual Machine (which has Docker preinstalled) or you can get docker [here](https://docs.docker.com/docker-for-windows/install/).
