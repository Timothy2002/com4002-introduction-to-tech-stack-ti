USE unibay;

# delete existing data before reinserting
DELETE FROM Bid WHERE id > 0;
DELETE FROM Item WHERE id > 0;
DELETE FROM Picture WHERE id > 0;
DELETE FROM User WHERE id > 0;
DELETE FROM Category WHERE id > 0;

# reset primary keys to increment from 1
ALTER TABLE User AUTO_INCREMENT = 1;
ALTER TABLE Picture AUTO_INCREMENT = 1;
ALTER TABLE Item AUTO_INCREMENT = 1;
ALTER TABLE Bid AUTO_INCREMENT = 1;
ALTER TABLE Category AUTO_INCREMENT = 1;

INSERT INTO User (email, username, password, is_admin) VALUES
  ("bob@bob.com", "scarface", "def123B-2", 0),
  ("goofy@hotmail.com", "goofy", "g00f8a11", 0),
  ("lala@tinkywinky.com", "lala", "ttsay88", 0),
  ("shushu@gmail.com", "shushu", "jiggy_66", 0),
  ("admin@ltucook.com", "admin", "pass123", 1);

INSERT INTO Category (name) VALUES
	("household"),
	("clothing"),
	("electrical"),
	("footwear"),
	("sports equipment");
    
INSERT INTO Item (title, description, asking_price, cat_id, user_id) VALUES
("Chelsea Boots", "Size 7 hardly worn.", 20.00, 4, 1),
("Yellow Skirt", "Size 10 lovely summery skirt.", 4.99, 2, 2),
("Grey Hoodie", "Size M. Unwanted gift. Organic cotton.", 15.00, 2, 3),
("HTC One Mini Unlocked", "Used phone in good used condition. Sim free. No memory included.", 99.99, 3, 5);

INSERT INTO Picture (item_id, url) VALUES
	(1, "https://images.topshop.com/i/TopShop/TS42K03TBLK_M_1.jpg?$w1300$&fmt=webp&qlt=80"),
	(2, "https://www.bodenimages.com/productimages/productxlarge/20wsum_t0573_dyl.jpg");
    
INSERT INTO Bid (bidder_id, item_id, amount) VALUES
	(4, 1, 9.50),
    (4, 2, 21.00),
	(3, 2, 13.20),
	(3, 1, 16.00),
	(5, 2, 3.10);
